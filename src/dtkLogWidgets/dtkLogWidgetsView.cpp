// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLogWidgetsView.h"

#include "dtkLogWidgetsView_p.h"

#include <dtkLog>

#include <QtCore>
#include <QtWidgets>

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsViewBar
// /////////////////////////////////////////////////////////////////

dtkLogWidgetsViewBar::dtkLogWidgetsViewBar(QWidget *parent) : QFrame(parent)
{
    QPushButton *b_trace = new QPushButton("Trace", this);
    QPushButton *b_debug = new QPushButton("Debug", this);
    QPushButton *b_info  = new QPushButton("Info", this);
    QPushButton *b_warn  = new QPushButton("Warn", this);
    QPushButton *b_error = new QPushButton("Error", this);
    QPushButton *b_fatal = new QPushButton("Fatal", this);

    b_trace->setCheckable(true);
    b_debug->setCheckable(true);
    b_info->setCheckable(true);
    b_warn->setCheckable(true);
    b_error->setCheckable(true);
    b_fatal->setCheckable(true);

    b_trace->setChecked(true);
    b_debug->setChecked(true);
    b_info->setChecked(true);
    b_warn->setChecked(true);
    b_error->setChecked(true);
    b_fatal->setChecked(true);

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->addWidget(b_trace);
    layout->addWidget(b_debug);
    layout->addWidget(b_info);
    layout->addWidget(b_warn);
    layout->addWidget(b_error);
    layout->addWidget(b_fatal);

    connect(b_trace, SIGNAL(clicked(bool)), this, SIGNAL(displayTrace(bool)));
    connect(b_debug, SIGNAL(clicked(bool)), this, SIGNAL(displayDebug(bool)));
    connect(b_info,  SIGNAL(clicked(bool)), this, SIGNAL(displayInfo(bool)));
    connect(b_warn,  SIGNAL(clicked(bool)), this, SIGNAL(displayWarn(bool)));
    connect(b_error, SIGNAL(clicked(bool)), this, SIGNAL(displayError(bool)));
    connect(b_fatal, SIGNAL(clicked(bool)), this, SIGNAL(displayFatal(bool)));
}

dtkLogWidgetsViewBar::~dtkLogWidgetsViewBar(void)
{

}

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsViewTree
// /////////////////////////////////////////////////////////////////

dtkLogWidgetsViewTree::dtkLogWidgetsViewTree(QWidget *parent) : QTreeWidget(parent)
{
    this->setAttribute(Qt::WA_MacShowFocusRect, false);
    this->setFrameShape(QFrame::HLine);
    this->setHeaderHidden(true);

    this->runtime = new QTreeWidgetItem(QStringList() << "Runtime log");

    this->file = new QTreeWidgetItem(QStringList() << "File log");
    this->file->addChild(new QTreeWidgetItem(QStringList() << dtkLogPath(qApp)));

    this->addTopLevelItem(this->runtime);
    this->addTopLevelItem(this->file);

    connect(this, SIGNAL(itemClicked(QTreeWidgetItem *, int)), this, SLOT(onItemClicked(QTreeWidgetItem *, int)));
}

dtkLogWidgetsViewTree::~dtkLogWidgetsViewTree(void)
{

}

void dtkLogWidgetsViewTree::onItemClicked(QTreeWidgetItem *item, int)
{
    if (item == this->runtime)
        emit runtimeClicked();
    else if (item == this->file)
        ;
    else
        emit fileClicked(item->text(0));
}

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsViewList
// /////////////////////////////////////////////////////////////////

dtkLogWidgetsViewList::dtkLogWidgetsViewList(QWidget *parent) : QListView(parent)
{
    this->model = new dtkLogModel(this);

    this->proxy = new QSortFilterProxyModel(this);
    this->proxy->setFilterKeyColumn(0);

    this->setModel(this->proxy);

    this->setAlternatingRowColors(true);
    this->setAttribute(Qt::WA_MacShowFocusRect, false);
    this->setFrameShape(QFrame::NoFrame);
    this->setRuntime();
}

dtkLogWidgetsViewList::~dtkLogWidgetsViewList(void)
{
    qDeleteAll(this->models);
}

void dtkLogWidgetsViewList::setAutoScroll(bool autoScroll)
{
    if (autoScroll)
        connect(this->model, SIGNAL(rowsInserted(const QModelIndex&, int, int)), this, SLOT(scrollToBottom()));
    else
        disconnect(this->model, SIGNAL(rowsInserted(const QModelIndex&, int, int)), this, SLOT(scrollToBottom()));
}

void dtkLogWidgetsViewList::setRuntime(void)
{
    dtkLogger::instance().attachModel(this->model);

    this->proxy->setSourceModel(this->model);
}

void dtkLogWidgetsViewList::setFile(const QString& path)
{
    if (this->models.contains(path))
        this->models.remove(path);

    QFile file(path);

    if (!file.open(QFile::ReadOnly))
        qDebug() << "Unable to read file" << path;

    QString contents = file.readAll();

    file.close();

    QStringListModel *model = new QStringListModel(contents.split("\n"));

    this->proxy->setSourceModel(model);

    this->models.insert(path, model);

    // QFileSystemWatcher *watcher = new QFileSystemWatcher(QStringList() << path, this);

    // connect(watcher, SIGNAL(fileChanged(const QString&)), this, SLOT(setFile(const QString&)));
}

void dtkLogWidgetsViewList::setFilter(const QRegExp& expression)
{
    this->proxy->setFilterRegExp(expression);
}

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsViewPrivate
// /////////////////////////////////////////////////////////////////

QRegExp dtkLogWidgetsViewPrivate::expression(void)
{
    if (this->exclude.isEmpty())
        return QRegExp();

    QString patterns;

    foreach (QString pattern, this->exclude)
        patterns.append(QString("%1|").arg(pattern));

    patterns.chop(1);

    return QRegExp(QString("^(?!%1).*").arg(patterns), Qt::CaseSensitive, QRegExp::RegExp2);
}

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsView
// /////////////////////////////////////////////////////////////////

dtkLogWidgetsView::dtkLogWidgetsView(QWidget *parent) : QWidget(parent), d(new dtkLogWidgetsViewPrivate)
{
    d->bar = new dtkLogWidgetsViewBar(this);

    d->tree = new dtkLogWidgetsViewTree(this);
    d->tree->setMaximumWidth(200);

    d->list = new dtkLogWidgetsViewList(this);

    d->checkbox_auto_scroll = new QCheckBox("Auto scroll", this);
    d->checkbox_auto_scroll->setTristate(false);

    QVBoxLayout *list_view_layout = new QVBoxLayout;
    list_view_layout->addWidget(d->checkbox_auto_scroll);
    list_view_layout->addWidget(d->list);

    QHBoxLayout *h_layout = new QHBoxLayout;
    h_layout->addWidget(d->tree);
    h_layout->addLayout(list_view_layout);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(d->bar);
    layout->addLayout(h_layout);

    connect(d->tree, SIGNAL(runtimeClicked()), d->list, SLOT(setRuntime()));
    connect(d->tree, SIGNAL(fileClicked(const QString&)), d->list, SLOT(setFile(const QString&)));

    connect(d->checkbox_auto_scroll, SIGNAL(stateChanged(int)), this, SLOT(autoScrollChecked(int)));
    connect(d->list, SIGNAL(clicked(const QModelIndex&)), this, SLOT(disableAutoScroll()));
    connect(d->bar, SIGNAL(displayTrace(bool)), this, SLOT(displayTrace(bool)));
    connect(d->bar, SIGNAL(displayDebug(bool)), this, SLOT(displayDebug(bool)));
    connect(d->bar, SIGNAL(displayInfo(bool)),  this, SLOT(displayInfo(bool)));
    connect(d->bar, SIGNAL(displayWarn(bool)),  this, SLOT(displayWarn(bool)));
    connect(d->bar, SIGNAL(displayError(bool)), this, SLOT(displayError(bool)));
    connect(d->bar, SIGNAL(displayFatal(bool)), this, SLOT(displayFatal(bool)));

    d->checkbox_auto_scroll->setChecked(true);
}

dtkLogWidgetsView::~dtkLogWidgetsView(void)
{
    delete d;

    d = NULL;
}

void dtkLogWidgetsView::autoScrollChecked(int state)
{
    if (state == Qt::Unchecked)
        this->disableAutoScroll();
    else
        this->enableAutoScroll();
}

void dtkLogWidgetsView::disableAutoScroll(void)
{
    d->list->setAutoScroll(false);
    d->checkbox_auto_scroll->setChecked(false);
}

void dtkLogWidgetsView::enableAutoScroll(void)
{
    d->list->setAutoScroll(true);
    d->checkbox_auto_scroll->setChecked(true);
}

void dtkLogWidgetsView::displayTrace(bool display)
{
    if (!display)
        d->exclude << "TRACE";
    else
        d->exclude.removeAll("TRACE");

    d->list->setFilter(d->expression());
}

void dtkLogWidgetsView::displayDebug(bool display)
{
    if (!display)
        d->exclude << "DEBUG";
    else
        d->exclude.removeAll("DEBUG");

    d->list->setFilter(d->expression());
}

void dtkLogWidgetsView::displayInfo(bool display)
{
    if (!display)
        d->exclude << "INFO";
    else
        d->exclude.removeAll("INFO");

    d->list->setFilter(d->expression());
}

void dtkLogWidgetsView::displayWarn(bool display)
{
    if (!display)
        d->exclude << "WARN";
    else
        d->exclude.removeAll("WARN");

    d->list->setFilter(d->expression());
}

void dtkLogWidgetsView::displayError(bool display)
{
    if (!display)
        d->exclude << "ERROR";
    else
        d->exclude.removeAll("ERROR");

    d->list->setFilter(d->expression());
}

void dtkLogWidgetsView::displayFatal(bool display)
{
    if (!display)
        d->exclude << "FATAL";
    else
        d->exclude.removeAll("FATAL");

    d->list->setFilter(d->expression());
}

//
// dtkLogWidgetsView.cpp ends here
