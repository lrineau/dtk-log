// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLogWidgetsExport>

#include <QtWidgets/QWidget>

class DTKLOGWIDGETS_EXPORT dtkLogWidgetsView : public QWidget
{
    Q_OBJECT

public:
     dtkLogWidgetsView(QWidget *parent = 0);
    ~dtkLogWidgetsView(void);

public slots:
    void displayTrace(bool display);
    void displayDebug(bool display);
    void displayInfo(bool display);
    void displayWarn(bool display);
    void displayError(bool display);
    void displayFatal(bool display);

protected slots:
    void autoScrollChecked(int state);
    void disableAutoScroll(void);
    void enableAutoScroll(void);

private:
    class dtkLogWidgetsViewPrivate *d;
};

//
// dtkLogWidgetsView.h ends here
