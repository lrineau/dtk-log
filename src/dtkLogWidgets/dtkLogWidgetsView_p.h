// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLogWidgetsExport>

#include <QtWidgets>

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsViewBar
// /////////////////////////////////////////////////////////////////

class DTKLOGWIDGETS_EXPORT dtkLogWidgetsViewBar : public QFrame
{
    Q_OBJECT

public:
     dtkLogWidgetsViewBar(QWidget *parent = 0);
    ~dtkLogWidgetsViewBar(void);

signals:
    void displayTrace(bool);
    void displayDebug(bool);
    void displayInfo(bool);
    void displayWarn(bool);
    void displayError(bool);
    void displayFatal(bool);
};

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsViewTree
// /////////////////////////////////////////////////////////////////

class DTKLOGWIDGETS_EXPORT dtkLogWidgetsViewTree : public QTreeWidget
{
    Q_OBJECT

public:
     dtkLogWidgetsViewTree(QWidget *parent = 0);
    ~dtkLogWidgetsViewTree(void);

signals:
    void runtimeClicked(void);
    void fileClicked(const QString& path);

protected slots:
    void onItemClicked(QTreeWidgetItem *, int);

private:
    QTreeWidgetItem *runtime;
    QTreeWidgetItem *file;
};

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsViewList
// /////////////////////////////////////////////////////////////////

class DTKLOGWIDGETS_EXPORT dtkLogWidgetsViewList : public QListView
{
    Q_OBJECT

public:
     dtkLogWidgetsViewList(QWidget *parent = 0);
    ~dtkLogWidgetsViewList(void);

public slots:
    void setRuntime(void);
    void setFile(const QString& path);
    void setAutoScroll(bool autoScroll);

public:
    void setFilter(const QRegExp& expression);

private:
    class dtkLogModel *model;

private:
    QHash<QString, QStringListModel *> models;

private:
    QSortFilterProxyModel *proxy;
};

// /////////////////////////////////////////////////////////////////
// dtkLogWidgetsViewPrivate
// /////////////////////////////////////////////////////////////////

class dtkLogWidgetsViewPrivate
{
public:
    QRegExp expression(void);

public:
    dtkLogWidgetsViewBar  *bar;
    dtkLogWidgetsViewTree *tree;
    dtkLogWidgetsViewList *list;

public:
    QStringList exclude;

public:
    QCheckBox *checkbox_auto_scroll;
};

//
// dtkLogWidgetsView_p.h ends here
