// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkLogLevel.h"
#include "dtkLogDestination.h"

#include <QtCore>

#include <iostream>
#include <streambuf>

// ///////////////////////////////////////////////////////////////////
// Helper class
// ///////////////////////////////////////////////////////////////////

namespace  dtk {
    class redirectStream : public std::basic_streambuf<char>
    {
    public:
        redirectStream(std::ostream& stream, LogLevel level) : m_stream(stream), m_level (level)
        {
            m_old_buf = stream.rdbuf();
            stream.rdbuf(this);
        }

        ~redirectStream(void)
        {
            m_stream.rdbuf(m_old_buf);
        }

    protected:
        int_type overflow(int_type v);
        std::streamsize xsputn(const char *p, std::streamsize n);

    private:
        std::ostream& m_stream;
        std::streambuf *m_old_buf;
        LogLevel m_level;
    };
}

// ///////////////////////////////////////////////////////////////////
// dtkLoggerPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkLoggerPrivate
{
public:
    dtk::LogLevel level;
    QHash<dtkLogDestination *, dtk::LogLevel> levels;

public:
    dtkLogDestinationPointer console;
    dtkLogDestinationPointer color_console;

public:
    QHash<QString,       dtkLogDestinationPointer> files;
    QHash<dtkLogModel *, dtkLogDestinationPointer> models;
    QHash<QWebSocket *,  dtkLogDestinationPointer> webSockets;

    QHash<QPair<const QObject *, const char *>, std::shared_ptr<dtkLogDestinationSlot>> m_slots;

public:
    QList<dtkLogDestinationPointer> destinations;

public:
    QMutex mutex;

public:
    dtk::redirectStream *cerr_stream;
    dtk::redirectStream *cout_stream;
};

//
// dtkLogger_p.h ends here
