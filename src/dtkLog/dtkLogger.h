// Version: $Id: 40ecb99ed408f4378dc257ab3e9156a3e2edd990 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLogExport>

#include <dtkLogLevel.h>
#include <dtkLogDestination.h>

#include <QtCore>

#include <iostream>

class dtkLogModel;
class QWebSocket;

#define DEFAULT_MAX_FILE_SIZE 1073741824L

class DTKLOG_EXPORT dtkLogger
{
public:
    static dtkLogger& instance(void);

    dtk::LogLevel level(void) const;
    QString levelString(void) const;

    void setLevel(dtk::LogLevel level);
    void setLevel(const QString& level_name);

    void attachConsole(bool color = false);
    void attachConsole(dtk::LogLevel level, bool color = false);
    void detachConsole(void);

    void attachWebSocket(QWebSocket *);
    void attachWebSocket(QWebSocket *, dtk::LogLevel);
    void detachWebSocket(QWebSocket *);

    void attachFile(const QString& path, qlonglong max_file_size = DEFAULT_MAX_FILE_SIZE);
    void attachFile(const QString& path, dtk::LogLevel, qlonglong max_file_size = DEFAULT_MAX_FILE_SIZE);
    void detachFile(const QString& path);

    void attachModel(dtkLogModel *);
    void attachModel(dtkLogModel *, dtk::LogLevel);
    void detachModel(dtkLogModel *);

    QMetaObject::Connection attachSlot(const QObject *receiver, const char *method, Qt::ConnectionType type = Qt::AutoConnection);
    void detachSlot(const QObject *receiver, const char *method);

    void redirectCout(dtk::LogLevel level = dtk::LogLevel::Info);
    void redirectCerr(dtk::LogLevel level = dtk::LogLevel::Error);

private:
     dtkLogger(void);
     dtkLogger(const dtkLogger&) = delete;
    ~dtkLogger(void);

    void write(const QHash<QString, QString>& message);
    void write(const QHash<QString, QString>& message, dtk::LogLevel level);

    dtkLogger& operator=(const dtkLogger&) = delete;

    class dtkLoggerPrivate *d;

    friend class dtkLogEngine;
    friend class dtkLogEnginePrivate;
};

//
// dtkLogger.h ends here
