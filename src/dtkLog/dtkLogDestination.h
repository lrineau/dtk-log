// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLogExport>

#include <QtCore>

#include <memory>

class dtkLogModel;

// ///////////////////////////////////////////////////////////////////
// dtkLogDestination interface
// ///////////////////////////////////////////////////////////////////

class DTKLOG_EXPORT dtkLogDestination
{
public:
    virtual ~dtkLogDestination(void) = default;

    virtual void write(const QHash<QString, QString>& message) = 0;
};

// ///////////////////////////////////////////////////////////////////
// dtkLogDestinationConsole declaration
// ///////////////////////////////////////////////////////////////////

class DTKLOG_EXPORT dtkLogDestinationConsole : public dtkLogDestination
{
public:
     dtkLogDestinationConsole(bool color = false);
    ~dtkLogDestinationConsole(void);

    void write(const QHash<QString, QString>& message) override;

private:
    class dtkLogDestinationConsolePrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// dtkLogDestinationFile declaration
// ///////////////////////////////////////////////////////////////////

class DTKLOG_EXPORT dtkLogDestinationFile : public dtkLogDestination
{
public:
     dtkLogDestinationFile(const QString& path, qlonglong max_file_size);
    ~dtkLogDestinationFile(void);

    void write(const QHash<QString, QString>& message) override;
    void setMaxFileSize(qlonglong size);

private:
    class dtkLogDestinationFilePrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// dtkLogDestinationModel declaration
// ///////////////////////////////////////////////////////////////////

class DTKLOG_EXPORT dtkLogDestinationModel : public dtkLogDestination
{
public:
     dtkLogDestinationModel(dtkLogModel *model);
    ~dtkLogDestinationModel(void);

public:
    void write(const QHash<QString, QString>& message) override;

private:
    class dtkLogDestinationModelPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// dtkLogDestinationWebSocket declaration
// ///////////////////////////////////////////////////////////////////

class QWebSocket;

class DTKLOG_EXPORT dtkLogDestinationWebSocket : public dtkLogDestination
{
public:
     dtkLogDestinationWebSocket(QWebSocket *socket);
    ~dtkLogDestinationWebSocket(void);

    void write(const QHash<QString, QString>& message) override;

private:
    class dtkLogDestinationWebSocketPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// dtkLogDestinationSlot declaration
// ///////////////////////////////////////////////////////////////////

class DTKLOG_EXPORT dtkLogDestinationSlot : public dtkLogDestination
{
public:
     dtkLogDestinationSlot(const QObject *receiver, const char *method, Qt::ConnectionType type);
    ~dtkLogDestinationSlot(void);

    void write(const QHash<QString, QString>& message) override;
    void disconnect(void);

    QMetaObject::Connection connection(void);

private:
    class dtkLogDestinationSlotPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// Helper type
// ///////////////////////////////////////////////////////////////////

using dtkLogDestinationPointer = std::shared_ptr<dtkLogDestination>;

//
// dtkLogDestination.h ends here
