// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLogEngine.h"

#include "dtkLogger.h"
#include "dtkLogger_p.h"

#include <functional>

// /////////////////////////////////////////////////////////////////
// dtkLogEnginePrivate declaration
// /////////////////////////////////////////////////////////////////

class dtkLogEnginePrivate
{
public:
    dtkLogEnginePrivate(dtk::LogLevel lev, bool cust);
    dtkLogEnginePrivate(dtk::LogLevel lev, const QLoggingCategory& cat);

    void writeLevel(void) const;
    void writeCategory(void) const;

    std::function<void()> write;

public:
    dtk::LogLevel level;
    QLoggingCategory *category;
    QDebug stream;
    QString buffer;
    bool custom;
};

dtkLogEnginePrivate::dtkLogEnginePrivate(dtk::LogLevel lev, bool cust) : level(lev), category(nullptr), stream(&buffer), custom(cust)
{
    write = [this]() { this->writeLevel(); };
}

dtkLogEnginePrivate::dtkLogEnginePrivate(dtk::LogLevel lev, const QLoggingCategory& cat) : level(lev), category(&(const_cast<QLoggingCategory&>(cat))), stream(&buffer), custom(false)
{
    write = [this]() { this->writeCategory(); };
}

void dtkLogEnginePrivate::writeLevel(void) const
{
    if (dtkLogger::instance().level() > this->level) {
        return;
    }

    QHash<QString, QString> message;
    message[QStringLiteral("level")] = dtk::logLevelToString(this->level);
    message[QStringLiteral("date")]  = QDateTime::currentDateTime().toString();
    message[QStringLiteral("buffer")] = this->buffer;

//    QString message = QString("[%1] - %2 - %3")
//        .arg(dtk::logLevelToString(this->level))
//        .arg(QDateTime::currentDateTime().toString())
//        .arg(this->buffer);

    QMutexLocker lock(&(dtkLogger::instance().d->mutex));

    if (!this->custom) {
        dtkLogger::instance().write(message);
    } else {
        dtkLogger::instance().write(message, level);
    }

    if (this->level ==  dtk::LogLevel::Fatal) {
        auto fatal_msg = QStringLiteral("Fatal error occured: ") + message["buffer"];
        qFatal("%s", qPrintable(fatal_msg));
    }
}

void dtkLogEnginePrivate::writeCategory(void) const
{
    QHash<QString, QString> message;
    message[QStringLiteral("category")] = category->categoryName();
    message[QStringLiteral("level")] = dtk::logLevelToString(this->level);
    message[QStringLiteral("date")]  = QDateTime::currentDateTime().toString();
    message[QStringLiteral("buffer")] = this->buffer;

    QMutexLocker lock(&(dtkLogger::instance().d->mutex));

    dtkLogger::instance().write(message);
}

// /////////////////////////////////////////////////////////////////
// dtkLogEngine implementation
// /////////////////////////////////////////////////////////////////

dtkLogEngine::dtkLogEngine(dtk::LogLevel level, bool custom) : d(new dtkLogEnginePrivate(level, custom))
{
}

dtkLogEngine::dtkLogEngine(dtk::LogLevel level, const QLoggingCategory& category) : d(new dtkLogEnginePrivate(level, category))
{
}

dtkLogEngine::~dtkLogEngine(void)
{
    d->write();
    delete d;
}

QDebug dtkLogEngine::stream(void) const
{
    return d->stream;
}

//
// dtkLogEngine.cpp ends here
