// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLogExport>

class QString;

namespace dtk {
    enum DTKLOG_EXPORT LogLevel {
        Trace  = 1 << 0, // 0b00000001
        Debug  = 1 << 1, // 0b00000010
        Info   = 1 << 2, // 0b00000100
        Warn   = 1 << 3, // 0b00001000
        Error  = 1 << 4, // 0b00010000
        Fatal  = 1 << 5  // 0b00100000
    };

    DTKLOG_EXPORT QString logLevelToString(LogLevel);
    DTKLOG_EXPORT LogLevel logLevelFromString(const QString&);
}

//
// dtkLogLevel.h ends here
