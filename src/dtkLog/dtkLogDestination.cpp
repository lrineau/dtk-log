// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <dtkLogDestination.h>

#include <dtkLogModel.h>
#include <dtkLogLevel.h>

#include <QtWebSockets>
#include <dtkLogFormatter.h>


// /////////////////////////////////////////////////////////////////
// dtkLogDestinationConsole implementation
// /////////////////////////////////////////////////////////////////

class dtkLogDestinationConsolePrivate
{
public:
       dtk::logFormatter formatter;
};

dtkLogDestinationConsole::dtkLogDestinationConsole(bool color) : d(new dtkLogDestinationConsolePrivate)
{
    if (!color) {
        d->formatter = dtk::logFormatterDefault;
    } else {
        d->formatter = dtk::logFormatterColor;
    }
}

dtkLogDestinationConsole::~dtkLogDestinationConsole(void)
{
    delete d;
}

void dtkLogDestinationConsole::write(const QHash<QString, QString>& message)
{
    fprintf(stderr, "%s\n", qPrintable(d->formatter(message)));
    fflush(stderr);
}

// /////////////////////////////////////////////////////////////////
// dtkLogDestinationFile implementation
// /////////////////////////////////////////////////////////////////

class dtkLogDestinationFilePrivate
{
public:
    QFile file;
    qlonglong max_file_size;

public:
    QTextStream stream;
};

dtkLogDestinationFile::dtkLogDestinationFile(const QString& path, qlonglong max_file_size) : d(new dtkLogDestinationFilePrivate)
{
    d->file.setFileName(path);
    d->max_file_size = max_file_size;

    QFileInfo info(path);

    QDir dir(info.absoluteDir());

    if (!dir.exists()) {
        QString name = dir.path();
        dir.mkpath(name);
    }

    if (!d->file.open(QFile::WriteOnly | QFile::Text | QIODevice::Append))
        qDebug() << "Unable to open" << path << "for writing";

    d->stream.setDevice(&(d->file));
}

dtkLogDestinationFile::~dtkLogDestinationFile(void)
{
    delete d;
}

void dtkLogDestinationFile::setMaxFileSize(qlonglong size)
{
    d->max_file_size = size;
}

void dtkLogDestinationFile::write(const QHash<QString, QString>& message)
{
    if (d->file.size() + message.size() > d->max_file_size) {
        qDebug() << "Max log file size reached" << d->max_file_size << ", rotate log file";
        d->file.flush();
        d->file.close();
        QString path = d->file.fileName();
        QString backup = path + ".old";

        // remove old backup
        if (QFile::exists(backup))
            QFile::remove(backup);

        QFile::rename( path, backup);

        if (!d->file.open(QFile::WriteOnly | QFile::Text | QIODevice::Append))
            qDebug() << "Unable to open" <<  path << "for writing";

    }

    d->stream << dtk::logFormatterDefault(message) << endl;
    d->stream.flush();
}

// /////////////////////////////////////////////////////////////////
// dtkLogDestinationModel implementation
// /////////////////////////////////////////////////////////////////

class dtkLogDestinationModelPrivate
{
public:
    dtkLogModel *model;
};

dtkLogDestinationModel::dtkLogDestinationModel(dtkLogModel *model) : d(new dtkLogDestinationModelPrivate)
{
    d->model = model;
}

dtkLogDestinationModel::~dtkLogDestinationModel(void)
{
    delete d;
}

void dtkLogDestinationModel::write(const QHash<QString, QString>& message)
{
    d->model->append( dtk::logFormatterDefault(message) );
}

// /////////////////////////////////////////////////////////////////
// dtkLogDestinationWebSocket implementation
// /////////////////////////////////////////////////////////////////

class dtkLogDestinationWebSocketPrivate
{
public:
    QWebSocket *socket;

public:
    QTextStream stream;
};

dtkLogDestinationWebSocket::dtkLogDestinationWebSocket(QWebSocket *socket) : d(new dtkLogDestinationWebSocketPrivate)
{
    d->socket = socket;
}

dtkLogDestinationWebSocket::~dtkLogDestinationWebSocket(void)
{
    delete d;
}

void dtkLogDestinationWebSocket::write(const QHash<QString, QString>& message)
{
    QJsonObject j_log;
    j_log.insert("type", "log");
    j_log.insert("message", dtk::logFormatterDefault(message) );

    QJsonDocument j_doc(j_log);
    d->socket->sendTextMessage(j_doc.toJson(QJsonDocument::Compact));
}

// /////////////////////////////////////////////////////////////////
// dtkLogDestinationSlot implementation
// /////////////////////////////////////////////////////////////////

class dtkLogDestinationSlotPrivate : public QObject
{
    Q_OBJECT

public:
    const QObject *receiver;
    const char *method;
    Qt::ConnectionType type;

    QMetaObject::Connection m_connection;

signals:
    void send(const QString& message);
};

dtkLogDestinationSlot::dtkLogDestinationSlot(const QObject *receiver, const char *method, Qt::ConnectionType type) : d(new dtkLogDestinationSlotPrivate)
{
    d->receiver = receiver;
    d->method   = method;
    d->type     = type;
    d->m_connection = QObject::connect(d, SIGNAL(send(const QString&)), receiver, method, type);
}

dtkLogDestinationSlot::~dtkLogDestinationSlot(void)
{   
    this->disconnect();
    delete d;
}

void dtkLogDestinationSlot::write(const QHash<QString, QString>& message)
{
    emit d->send(dtk::logFormatterDefault(message));
}

void dtkLogDestinationSlot::disconnect(void)
{
    QObject::disconnect(d->m_connection);
}

QMetaObject::Connection dtkLogDestinationSlot::connection(void)
{
    return d->m_connection;
}

#include "dtkLogDestination.moc"
//
// dtkLogDestination.cpp ends here
