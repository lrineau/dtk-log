// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkLogCategory.h"
#include "dtkLogEngine.h"
#include "dtkLogger.h"
#include "dtkLogLevel.h"

#include <QtCore>

// ///////////////////////////////////////////////////////////////////
// Basic Log functions declarations
// ///////////////////////////////////////////////////////////////////

dtkLogEngine dtkTrace(void);
dtkLogEngine dtkDebug(void);
dtkLogEngine dtkInfo (void);
dtkLogEngine dtkWarn (void);
dtkLogEngine dtkError(void);
dtkLogEngine dtkFatal(void);

dtkLogEngine dtkLog(dtk::LogLevel level);
dtkLogEngine dtkLog(const QLoggingCategory& cat, dtk::LogLevel level);

template <typename T> const dtkLogEngine& operator << (const dtkLogEngine& eng, const T& t);

QString dtkLogPath(QCoreApplication *application);

// ///////////////////////////////////////////////////////////////////
// Imlementations
// ///////////////////////////////////////////////////////////////////

inline dtkLogEngine dtkTrace(void) { return dtkLogEngine(dtk::LogLevel::Trace); }
inline dtkLogEngine dtkDebug(void) { return dtkLogEngine(dtk::LogLevel::Debug); }
inline dtkLogEngine dtkInfo (void) { return dtkLogEngine(dtk::LogLevel::Info ); }
inline dtkLogEngine dtkWarn (void) { return dtkLogEngine(dtk::LogLevel::Warn ); }
inline dtkLogEngine dtkError(void) { return dtkLogEngine(dtk::LogLevel::Error); }
inline dtkLogEngine dtkFatal(void) { return dtkLogEngine(dtk::LogLevel::Fatal); }

inline dtkLogEngine dtkLog(dtk::LogLevel level) { return dtkLogEngine(level, true); }

inline dtkLogEngine dtkLog(const QLoggingCategory& cat, dtk::LogLevel level = dtk::LogLevel::Info) { return dtkLogEngine(level, cat); }

template <typename T> inline const dtkLogEngine& operator << (const dtkLogEngine& eng, const T& t)
{
    eng.stream() << t;
    return eng;
}

inline QString dtkLogPath(QCoreApplication *application)
{
    return QDir(QStandardPaths::standardLocations(QStandardPaths::DataLocation).first()).filePath(QString("%1.log").arg(application->applicationName()));
}

//
// dtkLog.h ends here
