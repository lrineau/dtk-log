// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLogExport>

#include <QLoggingCategory>


DTKLOG_EXPORT const QLoggingCategory& dtkAll(void);
DTKLOG_EXPORT const QLoggingCategory& dtkCore(void);
DTKLOG_EXPORT const QLoggingCategory& dtkWidgets(void);
DTKLOG_EXPORT const QLoggingCategory& dtkVisualization(void);

#define DTK_CATEGORY_DECLARE(EXPORT, name, ...) \
    EXPORT const QLoggingCategory& name(__VA_ARGS__);

#define DTK_CATEGORY_DEFINE(name, string_id) \
    Q_LOGGING_CATEGORY(name, string_id);

namespace dtk {
    DTKLOG_EXPORT void setDefaultLogCategory(const QLoggingCategory& log_category);
    DTKLOG_EXPORT const QLoggingCategory& defaultLogCategory(void);
}

//
// dtkLogCategory.h ends here
