// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkLogLevelTestCase : public QObject
{
    Q_OBJECT

public:
     dtkLogLevelTestCase(void);
    ~dtkLogLevelTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testTrace(void);
    void testDebug(void);
    void testInfo(void);
    void testWarn(void);
    void testError(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkLogLevelTestCasePrivate *d;
};

//
// dtkLogLevelTest.h ends here
