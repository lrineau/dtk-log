// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkLogDestinationSlotTestCase : public QObject
{
    Q_OBJECT

public:
     dtkLogDestinationSlotTestCase(void);
    ~dtkLogDestinationSlotTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testConnection(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkLogDestinationSlotTestCasePrivate *d;
};

//
// dtkLogDestinationSlotTest.h ends here
