// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkLogCategoryTestCase : public QObject
{
    Q_OBJECT

public:
     dtkLogCategoryTestCase(void);
    ~dtkLogCategoryTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCategory(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkLogCategoryTestCasePrivate *d;
};

//
// dtkLogCategoryTest.h ends here
