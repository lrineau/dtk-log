// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLogDestinationSlotTest.h"

#include <dtkLogTest>

#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// dtkLogDestinationSlotTestCasePrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkLogDestinationSlotTestCasePrivate : public QObject
{
    Q_OBJECT
public:
    QString message;

public slots : 
    void receive(const QString&);
    

};

// ///////////////////////////////////////////////////////////////////

void dtkLogDestinationSlotTestCasePrivate::receive(const QString& message)
{
    this->message = message;
}

dtkLogDestinationSlotTestCase::dtkLogDestinationSlotTestCase(void) : d(new dtkLogDestinationSlotTestCasePrivate)
{
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
    d->message    = "None";

}

dtkLogDestinationSlotTestCase::~dtkLogDestinationSlotTestCase(void)
{
    delete d;
}

void dtkLogDestinationSlotTestCase::initTestCase(void)
{

}

void dtkLogDestinationSlotTestCase::testConnection(void)
{
    auto connection = dtkLogger::instance().attachSlot(d, SLOT(receive(const QString&)));
    QVERIFY(connection); // Connection must be valid

    QString m_message("Attach Slot");
    dtkDebug() << m_message;
    QVERIFY(d->message.contains(m_message));

    dtkLogger::instance().detachSlot(d, SLOT(receive(const QString&)));
    m_message = "Detach Slot";
    dtkDebug() << m_message;
    QVERIFY(!d->message.contains(m_message));
    QVERIFY(!connection); // Connection is not valid anymore

    // Test reconnection
    auto connection_2 = dtkLogger::instance().attachSlot(d, SLOT(receive(const QString&)));
    QVERIFY(!connection); // Connection is still the first instance so it is not valid
    QVERIFY(connection_2); // Connection_2 is the ne new one so it must be valid
    connection = connection_2;
    QVERIFY(connection); // now connection refers to the new connection and it s valid

    m_message = QStringLiteral("Attach Slot");
    dtkDebug() << m_message;
    QVERIFY(d->message.contains(m_message));

    dtkLogger::instance().detachSlot(d, SLOT(receive(const QString&)));
    m_message = "Detach Slot";
    dtkDebug() << m_message;
    QVERIFY(!d->message.contains(m_message));
    QVERIFY(!connection); // Connection is not valid anymore
}

void dtkLogDestinationSlotTestCase::init(void)
{

}

void dtkLogDestinationSlotTestCase::cleanupTestCase(void)
{

}

void dtkLogDestinationSlotTestCase::cleanup(void)
{

}

DTKLOGTEST_MAIN_NOGUI(dtkLogDestinationSlotTest, dtkLogDestinationSlotTestCase);

#include "dtkLogDestinationSlotTest.moc"

//
// dtkLogDestinationSlotTest.cpp ends here
