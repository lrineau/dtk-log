[[_TOC_]]

# dtk-log

Inserting log statements into your code not only is a very powerful debugging process but also makes the developer intent clearer, and helps collecting information once your application is manipulated by end users.

Many programming languages or frameworks such as Cocoa have native support for logging, other languages have their habits through third party libraries: apache’s log4j when it comes to java, log4cxx or log4c++ when it comes to c++. However, these libraries are can become quite huge for our purposes and increase the difficulty of maintaining a project regarding deployment and diffusion.

**dtk-log** layer is a very lightweight logging library that do not add any more dependency except Qt5 to your application while doing the job.

## Interface

dtk-log layer as part of dtk is built on top of Qt, and therefor features the same usage as qt’s basic output message functions such as `qDebug()` or `qFatal()`. This basically means that any type which supports `QDebug` operators is compatible with the following functions, found in dtkLog/dtkLog.h.

``` cpp
dtkTrace() << "Trace";
dtkDebug() << "Debug";
dtkInfo()  << "Info";
dtkWarn()  << "Warn";
dtkError() << "Error";
dtkFatal() << "Fatal";
```

Before using these functions, from anywhere within your source code, your application has to provide some information, such as the explicit application name, so that dtkLog destinations can be set up correctly.

``` cpp
#include <dtkLog>

QCoreApplication application(argc, argv);
application.setApplicationName("dtkLogger");

dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
dtkLogger::instance().attachConsole();
dtkLogger::instance().attachFile(dtkLogPath(&application));
```

Also, the logger has to be given a default level (***INFO*** is used by default otherwise), and destinations have to be attached to the log engine (no destination is attached by default).

## Levels

As for any respectable logging facility, messages are associated to a level, which allows classification and later filtering of log messages.

| Level | Definition |
|:-----:|:-----------|
| **FATAL**| Severe errors that cause premature termination. |
| **ERROR**| Runtime errors or unexpected conditions. |
| **WARN** | Use of deprecated APIs, undesirable but not necessarily wrong runtimes situations. |
| **INFO** | Interesting runtime events. |
| **DEBUG**| Detailed information on the flow through the system. |
| **TRACE**| More detailed information about the code path. |

These levels are addressed by choosing the relevant entry point in dtk log’s interface.

Note that these level form a hierarchy. If dtk’s logging level is set to *TRACE*, all logs will be visible, unless set to *WARN*, only *WARN*, *ERROR* and *FATAL* log messages will be visible.

## Categories

Categories allow to split the logs to make them more understandable. For example, the logs could be categorized as 
"core", "network", and "db" to split, following the application main components.

### Declaration
```
```

### Definition
```
DTK_CATEGORY_DEFINE(myCategory, "my.category");
```
Defines the `myCategory()` helper, and states that comments in the category use the "my.category" string.
### Usage
* By default, the messages for a category are of info level:
```
dtkLog(myCategory()) << "category, with defaut level (info)";
```
The previous line produces the following output:
```
[my.category][INFO ] - Wed Dec 16 11:14:14 2020 - "category, with defaut level (info)" 
```
* The default level can be changed:
```
dtkLog(myCategory(), dtk::Fatal) << "category, with fatal level";
```
Where dtk::Fatal can be replace by any value of dtk::LogLevel. The previous line produces
the following output:
```
[my.category][FATAL] - Wed Dec 16 11:14:14 2020 - "category, with fatal level" 
```

## Destinations

dtk’s log level currently provides four destinations: console, file, model and websocket.

Console destination outputs to the standard error chanel and flushes it. File destination, retrieved from Qt’s data storage location, outputs messages by appending them to the file stream in write only mode.

Model destination is a dtk log specialization of a QAbstractListModel, that, together with a view, lets developers embed a widget displaying log events directly into any dtk based application.

Websocket destination is **TO COMPLETE**
