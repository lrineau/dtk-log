# ChangeLog
## version 2.2.0 - 2019-05-16
- cmake refactoring
## version 2.1.7 - 2019-02-22
- fix install rules of dtkLogWidgets
## version 2.1.6 - 2018-12-04
- remove nospace in << operator
## version 2.1.5 - 2018-09-14
- fix cmake config files for dtkLogWidgets
## version 2.1.4 - 2018-09-11
- fix install PATH for windows
## version 2.1.3 - 2018-09-10
- fix missing models
## version 2.1.2 - 2018-09-10
- fix EXPORT headers
